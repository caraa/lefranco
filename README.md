# Franco
Logiciel de lecture des fichiers .frl
# Qu'est-ce que le Franco ?
Le Franco est un langage de programmation révolutionnaire entièrement francophone.
Il a été conçu pour les personnes souhaitant se lancer dans la programmation informatique, mais qui ont peur d'un langage anglophone traditionnel.

# Comment exécuter un fichier .frl ?
# Sous Linux (Debian)

1. Entrez dans le répertoire src du repository :
cd Franco/sources

2. Entrez la commande suivante :
python3 franco-reader.py <NOM_DE_VOTRE_FICHIER>.frl


# Comment installer le lecteur Franco ?
# Sous Linux (Debian)

1. Entrez dans le répertoire sources du repository
cd Franco/sources

2. Rendez le Lecteur Franco exécutable
sudo chmod +x franco-reader.py

3. Copiez le script dans le dossier /usr/local/bin
sudo cp franco-reader.py /usr/local/lib/franco-reader

4. Testez le lecteur en ligne de commande.
franco-reader <NOM_DE_VOTRE_FICHIER>.frl
